export const mockData = [
  {
    name: "Fruit & vegetables",
    display: true,
    children: [
      {
        name: "Fruit",
        display: false,
        children: [
          {
            name: "Anti-oxidant fruit (berries, grapes)",
            display: true,
            children: [],
          },
        ],
      },
      {
        name: "Vegetables",
        display: false,
        children: [],
      },
    ],
  },
  {
    name: "Starchy food (carbohydrates)",
    display: true,
    children: [
      {
        name: "Potatoes",
        display: false,
        children: [],
      },
      {
        name: "Bread",
        display: false,
        children: [
          {
            name: "Whole grain bread",
            display: true,
            children: [],
          },
          {
            name: "White bread",
            display: false,
            children: [],
          },
        ],
      },
      {
        name: "Rice & grains",
        display: false,
        children: [
          {
            name: "Rice",
            display: false,
            children: [],
          },
          {
            name: "Grains (oats, barley, rye)",
            display: true,
            children: [],
          },
        ],
      },
      {
        name: "Pasta",
        display: false,
        children: [
          {
            name: "Whole grain pasta",
            display: true,
            children: [],
          },
          {
            name: "White pasta",
            display: false,
            children: [],
          },
        ],
      },
    ],
  },
  {
    name: "Dairy",
    display: true,
    children: [
      {
        name: "Milk",
        display: false,
        children: [],
      },
      {
        name: "Yogurt",
        display: false,
        children: [],
      },
      {
        name: "Cheese",
        display: false,
        children: [],
      },
    ],
  },
  {
    name: "Protein food",
    display: true,
    children: [
      {
        name: "Pulses (beans, peas and lentils)",
        display: true,
        children: [],
      },
      {
        name: "Fish",
        display: false,
        children: [
          {
            name: "Oil-rich fish",
            display: true,
            children: [],
          },
          {
            name: "White fish & shellfish",
            display: false,
            children: [],
          },
        ],
      },
      {
        name: "Eggs",
        display: false,
        children: [],
      },
      {
        name: "Meat",
        display: false,
        children: [
          {
            name: "White meat",
            display: false,
            children: [],
          },
          {
            name: "Red meat",
            display: false,
            children: [],
          },
          {
            name: "Processed meat",
            display: false,
            children: [],
          },
        ],
      },
    ],
  },
  {
    name: "Fat",
    display: true,
    children: [
      {
        name: "Oils & spreads",
        display: true,
        children: [],
      },
      {
        name: "Confectionery (aka sugary foods)",
        display: false,
        children: [],
      },
    ],
  },
];
