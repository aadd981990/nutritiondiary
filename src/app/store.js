import { configureStore } from "@reduxjs/toolkit";
import datesReducer from "../features/dates/datesSlice";

export default configureStore({
  reducer: {
    dates: datesReducer,
  },
});
