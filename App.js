import React, { Component } from "react";
import WixCalendar from "./components/WixCalendar";
import NutritionDiaryList from "./components/NutritionDiaryList";
import { StyleSheet, Text, Modal, Pressable, View } from "react-native";
import { Button } from "react-native-web";
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      datesData: {
        "2022-04-16": {
          selected: true,
          marked: true,
          selectedColor: "blue",
        },
        "2022-04-17": {
          selected: true,
          marked: true,
          selectedColor: "blue",
        },
        "2022-04-19": {
          selected: true,
          marked: true,
          selectedColor: "blue",
        },
        modalVisible: false,
        lastDate: "",
      },
      editInfo: {},
      info: { food: [] },
    };
  }

  setModalVisible = (visible) => {
    this.setState({ modalVisible: visible });
  };
  setLastDate = (lD) => {
    this.setState({ lastDate: lD });
  };

  setEditInfo = (food) => {
    this.setState({ editInfo: food });
  };

  actionInMondal = (selectedDate) => {
    this.setState((state, props) => ({
      datesData: {
        ...state.datesData,
        [selectedDate]: {
          selected: true,
          marked: true,
          selectedColor: "blue",
          info: state.info.food,
        },
      },
    }));
    this.setLastDate(selectedDate);
  };

  changeDates = (selectedDate) => {
    this.setModalVisible(true);
    this.setLastDate(selectedDate);
  };
  editInfo = (pickfood) => {
    const { info } = this.state;
    if (info.food.includes(pickfood) == false) {
      this.setState((state, props) => ({
        info: {
          food: [...state.info.food, pickfood],
        },
      }));
    } else {
      this.setState((state, props) => ({
        info: {
          food: state.info.food.filter((value) => value != pickfood),
        },
      }));

      let filtered = info.food;
      info.food = filtered;
    }
  };

  render() {
    const { modalVisible, lastDate, info } = this.state;
    //this.setModalVisible(false);
    return (
      <>
        <WixCalendar
          dataDates={this.state.datesData}
          changeDates={this.changeDates}
        ></WixCalendar>
        <View style={styles.centeredView}>
          <Modal
            animationType="slide"
            transparent={true}
            visible={modalVisible}
          >
            <View style={styles.centeredView}>
              <View style={styles.modalView}>
                <Text style={styles.modalText}>{lastDate}</Text>
                <NutritionDiaryList
                  editInfo={this.editInfo}
                ></NutritionDiaryList>
                <Pressable
                  style={[styles.button, styles.buttonClose]}
                  onPress={() => this.setModalVisible(!modalVisible)}
                >
                  <Text style={styles.textStyle}>
                    to close pressed several times
                  </Text>
                </Pressable>

                <Pressable
                  style={[styles.button, styles.buttonClose]}
                  onPress={() => this.actionInMondal(lastDate)}
                >
                  <Text style={styles.textStyle}>to add food on that date</Text>
                </Pressable>

                {info.food.map((food, index) => (
                  <Text key={index}>{food}</Text>
                ))}
              </View>
            </View>
          </Modal>
        </View>
      </>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  button: {
    alignItems: "center",
    backgroundColor: "#DDDDDD",
    padding: 10,
    marginBottom: 10,
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  buttonOpen: {
    backgroundColor: "#F194FF",
  },
  buttonClose: {
    backgroundColor: "#2196F3",
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center",
  },
});
export default App;
