import React from "react";
import { View, Text } from "react-native";

class NutritionDiary extends React.Component {
  render() {
    const { diary, isChild, editInfo } = this.props;
    let info = { food: [] };

    return (
      <>
        {diary.display && (
          <Text
            onPress={() => {
              editInfo(diary.name);
            }}
          >
            {diary.name}
          </Text>
        )}
        {diary.children.map((child, index) => (
          <NutritionDiary
            editInfo={editInfo}
            diary={child}
            key={index}
            isChild
          />
        ))}
      </>
    );
  }
}

export default NutritionDiary;
