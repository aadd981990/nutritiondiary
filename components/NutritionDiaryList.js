import React from "react";
import { mockData } from "../data/NutritionDiary";
import NutritionDiary from "../components/NutritionDiary";

class NutritionDiaryList extends React.Component {
  render() {
    console.log(this.props);
    const { editInfo } = this.props;
    return (
      <>
        {mockData.map((diary, index) => (
          <NutritionDiary editInfo={editInfo} diary={diary} key={index} />
        ))}
      </>
    );
  }
}

export default NutritionDiaryList;
